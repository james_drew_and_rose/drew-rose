<?php
/*
     Template Name: Homepage
*/

get_header();
?>

 <section class="home-video container">
      <div class="content">
      </div>
    </section>

    <div id="set-height"></div>

    <video id="v0" tabindex="0", autobuffer preload>
      <source type='video/mp4;codecs="avc1.42E01E, mp4a.40.2"' src="https://drewandrose.com/wp-content/uploads/2022/04/DrewRose-11-2.m4v"></source>
    </video>
<main id="site-content" role="main">
	<?php
	if ( have_posts() ) {while ( have_posts() ) {the_post();

			/*the_title( '<h1 class="entry-title">', '</h1>' );*/
			the_content();
			the_post_thumbnail( 'full' );

	}}?>
</main>



<?php get_footer(); ?>