<?php
/*
     Template Name: Resources
*/

get_header();
?>

<section class="blog-resouces-podcasts-ctn resources-main">

     <!-- HERO -->
     <div class="wp-block-group hero">
          <div class="wp-block-group__inner-container">
               <div class="wp-block-columns">
                    <div class="wp-block-column">
                         <h1 class="hidden-page-title">Resources</h1>
                         <div class="insights-hero">
                              <?php echo file_get_contents( get_template_directory_uri() . '/images/insights-resources.svg' ); ?>
                         </div>
                    </div>
               </div>
          </div>
     </div>


     <div class="wp-block-group blog-ctn">
          <div class="wp-block-group__inner-container">
               <?php $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                    $query = new WP_Query( array(
                    'post_type' => 'resources',
                    'post_status'  => 'publish',
                    'orderby' =>  'title',
                    'order' =>  'ASC',
                    'posts_per_page' => -1
                    ) );
               ?>

               <?php if ( $query->have_posts() ) : $count = 0; ?>

               <?php while ( $query->have_posts() ) : $query->the_post();
                    $count ++;
                    $even_odd_class = ( ($count % 2) == 0 ) ? "column-lines-container-2 even" : "column-lines-container-1 odd";
                    $image_border_class = ( ($count % 2) == 0 ) ? "curved-img-border-left" : "curved-img-border-right";
               ?>
               <a class="single-blog-link" target="_blank" href="<?php echo get_the_excerpt(); ?>"
                    title="<?php the_title(); ?>" aria-label="Read more about <?php the_title(); ?>">
                    <div class="single-blog-item">
                         <?php if ( has_post_thumbnail() ) : ?>
                         <div class="card-img-top blog-img-col"
                              style="background-image:url(<?php the_post_thumbnail_url(); ?>);">
                         </div>
                         <?php endif; ?>
                         <div class="content-col">
                              <p class="blog-date"><?php echo get_the_date(); ?></p>
                              <h2><a target="_blank" href="<?php echo get_the_excerpt(); ?>"
                                        title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
                              <div class="blog-excerpt"><?php the_content(); ?></div>
                         </div>
                    </div>
               </a>


               <?php endwhile; ?>

               <?php wp_reset_postdata(); ?>

               <?php else : ?>
               <div class="alert alert-warning">
                    <?php _e( 'Sorry, no posts matched your criteria.' ); ?>
               </div>
               <?php endif; ?>
          </div>
     </div>


</section>

<?php get_footer(); ?>