<?php

/*
    Template Name: Offline 5 sections
    Template Post Type: our-work
*/

get_header();
?>
<main id="site-content" role="main">
     <div class="wrapper">
          <?php
    	if ( have_posts() ) {while ( have_posts() ) {the_post();

                the_title( '<h1 class="entry-title">', '</h1>' );
                the_post_thumbnail( 'full' );
                echo( '<div class="content-section">' );
    			the_content();
                echo( '</div>' );

    	}}?>
     </div>
     <?php
        $post_id = get_the_ID();
		$query_args = array(
            'tax_query' => array(
                'relation' => 'AND',
                array(
                    'taxonomy' => 'our-work-category',
                    'field'    => 'slug',
                    'terms'    => array( 'offline', 'online' ),
                ),
            ),
		    'post_type'      => 'our-work',
		    'posts_per_page'  => -1,
            'post__not_in'    => array($post_id),
		    'orderby' => 'rand',
		);
		$related_cats_post = new WP_Query( $query_args );
        ?>
     <?php if($related_cats_post->have_posts()): ?>
     <div class="post-related">
          <div class="wrapper">
               <div id="related-portfolio-carousel" class="portfolio-posts">
                    <?php
                    while($related_cats_post->have_posts()): $related_cats_post->the_post();
                    ?>
                    <article class="latest-portfolio-box">
                         <div class="portfolio-post-inner">
                              <?php if ( has_post_thumbnail() ) : ?>
                              <div class="portfolio-post-img">
                                   <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"
                                        aria-label="Read more about <?php the_title(); ?>"><?php the_post_thumbnail('large'); ?></a>
                              </div>
                              <?php endif; ?>
                              <div class="portfolio-post-content">
                                   <div class="portfolio-post-title">
                                        <h3><a href="<?php the_permalink(); ?>"
                                                  title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
                                   </div>
                              </div>
                         </div>
                    </article>
                    <?php endwhile; ?>
               </div>
               <?php wp_reset_postdata(); ?>
          </div>
     </div>
     <?php endif; ?>
</main>


<?php get_footer(); ?>