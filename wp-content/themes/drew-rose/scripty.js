(function ($) {
  // Burger Nav
  $(".burger").click(function () {
    $(".burger").toggleClass("active");
    $(".nav.menu").toggleClass("active");
  });

  // Testimonial slider
  $(".testimonial-slider").slick({
    dots: true,
    arrows: false,
  });
  // Testimonial slider
  $(".casestudy-slider .wp-block-group__inner-container").slick({
    dots: true,
    arrows: false,
  });

  // Prices slider
  if (window.matchMedia("(max-width: 768px)").matches) {
  /* the viewport is less than 768 pixels wide */
    $(".page-pricing .wp-block-columns").slick({
      dots: true,
      arrows: false,
    });
  };

   // Mobile sub menu slider
   $(".mobile-sub-menu").slick({
    dots: true,
    arrows: false,
  });

  $("#related-portfolio-carousel").slick({
    dots: false,
    autoplay: true,
    autoplaySpeed: 8000,
    infinite: false,
    speed: 800,
    slidesToShow: 2,
    slidesToScroll: 2,
    prevArrow:
      "<button type='button' class='slick-prev' data-role='none' role='button' aria-label='Previous' tabindex='0'></button>",
    nextArrow:
      "<button type='button' class='slick-next' data-role='none' role='button' aria-label='Next' tabindex='0'></button>",
    arrows: true,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          dots: false,
        },
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  });

  // What we do online toggle read more
  $(".read-more-engage").click(function () {
    $(".hidden-content-engage").slideToggle(300);
    $(".read-more-engage").hide();
  });
  $(".read-less-engage").click(function () {
    $(".hidden-content-engage").slideToggle(300);
    $(".read-more-engage").show();
  });

  $(".read-more-convert").click(function () {
    $(".hidden-content-convert").slideToggle(300);
    $(".read-more-convert").hide();
  });
  $(".read-less-convert").click(function () {
    $(".hidden-content-convert").slideToggle(300);
    $(".read-more-convert").show();
  });

  $(".read-more-advocate").click(function () {
    $(".hidden-content-advocate").slideToggle(300);
    $(".read-more-advocate").hide();
  });
  $(".read-less-advocate").click(function () {
    $(".hidden-content-advocate").slideToggle(300);
    $(".read-more-advocate").show();
  });

  // blog posts related arrows
  $("#blog-related-posts").slick({
    dots: false,
    autoplay: true,
    autoplaySpeed: 8000,
    infinite: false,
    speed: 800,
    slidesToShow: 2,
    slidesToScroll: 2,
    prevArrow:
      "<button type='button' class='slick-prev' data-role='none' role='button' aria-label='Previous' tabindex='0'></button>",
    nextArrow:
      "<button type='button' class='slick-next' data-role='none' role='button' aria-label='Next' tabindex='0'></button>",
    arrows: true,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          dots: false,
        },
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  });

  //Bg animation
  // $( document ).ready(function($) {
  //     $('.wp-block-cover__video-background').removeAttr('loop');
  // });

  const video = document.querySelector(".wp-block-cover__video-background");

  //video.get(0).play();
  // $(".page-home .hero-video .wp-block-button a").css("display", "inline-block");
  // video.on("ended", function () {
  //   $(".page-home .hero-video .wp-block-button a").css(
  //     "display",
  //     "inline-block"
  //   );
  // });

  if (video) {
    video.addEventListener("timeupdate", function () {
      if (this.currentTime > 5.2 && this.currentTime < 11) {
        $(".page-home .hero-video .wp-block-button a").css(
          "display",
          "inline-block"
        );
      } else {
        $(".page-home .hero-video .wp-block-button a").css("display", "none");
      }
    });
  }

  //Homepage form submission

  $('#drewrose-homepage-form').submit(function(e){
    $('#drewrose-homepage-form').html('<p>Thanks for signing up!</p>');
     e.preventDefault();
});


  //On Pricing page to turn funnel blocks into links
 jQuery( '.turnmetolink-engage .wp-block-group__inner-container' ).wrapInner('<a href="https://drewandrose.com/online/#engage"></a>');
 jQuery( '.turnmetolink-convert .wp-block-group__inner-container' ).wrapInner('<a href="https://drewandrose.com/online/#convert"></a>');
 jQuery( '.turnmetolink-advocacy .wp-block-group__inner-container' ).wrapInner('<a href="https://drewandrose.com/online/#advocacy"></a>');


  console.log("Welcome to Drew & Rose");


      var frameNumber = 0, // start video at frame 0
    // lower numbers = faster playback
    playbackConst = 1000, 
    // get page height from video duration
    setHeight = document.getElementById("set-height"), 
    // select video element         
    vid = document.getElementById('v0'); 
    // var vid = $('#v0')[0]; // jquery option

  // dynamically set the page height according to video length
  vid.addEventListener('loadedmetadata', function() {
    setHeight.style.height = Math.floor(vid.duration) * playbackConst + "px";
  });
  // Use requestAnimationFrame for smooth playback
  function scrollPlay(){  
    var frameNumber  = window.pageYOffset/playbackConst;
    vid.currentTime  = frameNumber;
    window.requestAnimationFrame(scrollPlay);
  }
  window.requestAnimationFrame(scrollPlay);

})(jQuery);
