<?php

/*
    Template Name: What Is Ux Ui Page
*/

get_header();
?>

<main id="site-content" role="main">
     <div class="single-resource-header">
          <div class="wrapper">
               <div class="single-blog-item">
                    <div class="card-img-top blog-img-col"
                         style="background-image:url(<?php echo get_site_url(); ?>/wp-content/uploads/2022/04/resources-images-01.png);">
                    </div>
                    <div class="content-col">
                         <p class="blog-date">10 January 2022</p>
                         <h2>What is UX and UI?</h2>
                         <div class="blog-excerpt">User experience and interface design are the main focuses of digital
                              design today. Often confused, these two areas of design are closely related to each other
                              and cover many different fields and processes.</div>
                    </div>
               </div>
          </div>
     </div>

     <section class="timeline-block" id="timeline-block_61bc7d453b3ce">
          <div id="cd-timeline" class="container">
               <div class="cd-timeline-block-heading" style="background-color: #2F354D;">
                    <div class="cd-timeline-block-heading-title">
                         <h2>(UX) User Experience</h2>
                    </div>
                    <div class="cd-timeline-block-heading-copy">
                         <p class="mb-0">Also known as, Digital Product Design, is concerned
                              with what we trying to achieve or solve with a focus on interactions.</p>
                    </div>
               </div>
               <div class="cd-timeline-block counter-1">
                    <div class="cd-timeline-img cd-picture">
                         <img src="<?php echo get_template_directory_uri(); ?>/images/circle-left-line.svg"
                              alt="Circle">
                         <img class="circle-no-line"
                              src="<?php echo get_template_directory_uri(); ?>/images/circle-no-line.svg" alt="Circle">
                    </div> <!-- cd-timeline-img -->
                    <div class="cd-timeline-content">
                         <img src="<?php echo get_template_directory_uri(); ?>/images/user-research-icon.svg"
                              alt="Image">
                         <h2>User Research</h2>
                         <p>A combination of research methods can be used to effectively collect
                              data from clients and users, normally in a workshop, to help us understand the problem and
                              the user’s needs. From the data, we can group different user archetypes that represent the
                              most common users of your digital product.</p>
                    </div> <!-- cd-timeline-content -->
               </div>
               <div class="cd-timeline-block counter-2">
                    <div class="cd-timeline-img cd-picture">
                         <img src="<?php echo get_template_directory_uri(); ?>/images/circle-right-line.svg"
                              alt="Circle">
                         <img class="circle-no-line"
                              src="<?php echo get_template_directory_uri(); ?>/images/circle-no-line.svg" alt="Circle">
                    </div> <!-- cd-timeline-img -->
                    <div class="cd-timeline-content">
                         <img src="<?php echo get_template_directory_uri(); ?>/images/user-journeys-icon.svg"
                              alt="Image">
                         <h2>User Journeys</h2>
                         <p>From the user research, and referring back to our user archetypes, we can create user
                              journeys which will summarise how a user may interact with the design. These are useful as
                              the design can be tested against these journeys and the archetypes ensuring that the
                              product fulfils the user’s needs.</p>
                    </div> <!-- cd-timeline-content -->
               </div>
               <div class="cd-timeline-block counter-1 counter-3">
                    <div class="cd-timeline-img cd-picture">
                         <img class="circle-no-line"
                              src="<?php echo get_template_directory_uri(); ?>/images/circle-no-line.svg" alt="Circle">
                         <img src="<?php echo get_template_directory_uri(); ?>/images/circle-left-line.svg"
                              alt="Circle">
                    </div> <!-- cd-timeline-img -->
                    <div class="cd-timeline-content">
                         <img src="<?php echo get_template_directory_uri(); ?>/images/information-a-icon.svg"
                              alt="Image">
                         <h2>Information Architecture</h2>
                         <p>The website’s (product’s) content can now be organised, structured
                              and labeled – focusing on clarity for the user and easy navigation. This usually takes the
                              form of a site map.</p>
                    </div> <!-- cd-timeline-content -->
               </div>
               <div class="cd-timeline-block counter-2 counter-4">
                    <div class="cd-timeline-img cd-picture">
                         <img class="circle-no-line"
                              src="<?php echo get_template_directory_uri(); ?>/images/circle-no-line.svg" alt="Circle">
                         <img src="<?php echo get_template_directory_uri(); ?>/images/circle-right-line.svg"
                              alt="Circle">
                    </div> <!-- cd-timeline-img -->
                    <div class="cd-timeline-content">
                         <img src="<?php echo get_template_directory_uri(); ?>/images/wireframes-icon.svg" alt="Image">
                         <h2>Wireframes</h2>
                         <p>Now that we have an overall structure and we know what our user’s
                              need, the design work starts in low resolution wireframes to map out content and page
                              layout. Wireframes are often designed in greyscale and are not the finished product! Their
                              purpose is to show the bones/framework of the website to confirm content without any
                              visual or stylistic elements; that part comes later with the UI (User Interface) design.
                         </p>
                    </div> <!-- cd-timeline-content -->
               </div>
          </div>
     </section>

     <section class="timeline-block" id="timeline-block_61bc7db8207e1">
          <div id="cd-timeline" class="container">
               <div class="cd-timeline-block-heading" style="background-color: #007C83">
                    <div class="cd-timeline-block-heading-title">
                         <h2 style="color: #f7f2ee">(UI) User <br> Interface</h2>
                    </div>
                    <div class="cd-timeline-block-heading-copy">
                         <p class="mb-0" style="color: #f7f2ee">Also known as, Visual Design, is focused on tools and
                              the look of the design.</p>
                    </div>
               </div>
               <div class="cd-timeline-block counter-1">
                    <div class="cd-timeline-img cd-picture">
                         <img class="circle-no-line"
                              src="<?php echo get_template_directory_uri(); ?>/images/circle-no-line.svg" alt="Circle">
                         <img src="<?php echo get_template_directory_uri(); ?>/images/circle-left-line.svg"
                              alt="Circle">
                    </div> <!-- cd-timeline-img -->
                    <div class="cd-timeline-content">
                         <img src="<?php echo get_template_directory_uri(); ?>/images/visual-design-icon.svg"
                              alt="Image">
                         <h2>Visual Design</h2>
                         <p>From the wireframes we can now start to iterate on the design by working on the design
                              components as well as the graphic and stylistic elements to the website such as
                              typography, colour palette and imagery.</p>
                    </div> <!-- cd-timeline-content -->
               </div>
               <div class="cd-timeline-block counter-2">
                    <div class="cd-timeline-img cd-picture">
                         <img class="circle-no-line"
                              src="<?php echo get_template_directory_uri(); ?>/images/circle-no-line.svg" alt="Circle">
                         <img src="<?php echo get_template_directory_uri(); ?>/images/circle-right-line.svg"
                              alt="Circle">
                    </div> <!-- cd-timeline-img -->
                    <div class="cd-timeline-content">
                         <img src="<?php echo get_template_directory_uri(); ?>/images/layout-spacing-icon.svg"
                              alt="Image">
                         <h2>Layout & Spacing</h2>
                         <p>Specific layouts and spacing are also a part of the visual design, combining spacing with
                              elements and type can lead to a harmonious feeling across the product e.g. the distance
                              between images, text and buttons is a considered process as each pixel is taken into
                              account.</p>
                    </div> <!-- cd-timeline-content -->
               </div>
               <div class="cd-timeline-block counter-1">
                    <div class="cd-timeline-img cd-picture">
                         <img class="circle-no-line"
                              src="<?php echo get_template_directory_uri(); ?>/images/circle-no-line.svg" alt="Circle">
                         <img src="<?php echo get_template_directory_uri(); ?>/images/circle-left-line.svg"
                              alt="Circle">
                    </div> <!-- cd-timeline-img -->
                    <div class="cd-timeline-content">
                         <img src="<?php echo get_template_directory_uri(); ?>/images/colours-icon.svg" alt="Image">
                         <h2>Colours</h2>
                         <p>Colour studies are usually the most well known part of branding. Colours carry a lot of
                              meaning but must also be accessible and easily recognisable.</p>
                    </div> <!-- cd-timeline-content -->
               </div>
               <div class="cd-timeline-block counter-2">
                    <div class="cd-timeline-img cd-picture">
                         <img class="circle-no-line"
                              src="<?php echo get_template_directory_uri(); ?>/images/circle-no-line.svg" alt="Circle">
                         <img src="<?php echo get_template_directory_uri(); ?>/images/circle-right-line.svg"
                              alt="Circle">
                    </div> <!-- cd-timeline-img -->
                    <div class="cd-timeline-content">
                         <img src="<?php echo get_template_directory_uri(); ?>/images/typography-icon.svg" alt="Image">
                         <h2>Typography</h2>
                         <p>Type can be a specific focus in the product’s visuals, as text based content dominates most
                              pages, it’s important to have typography that’s easy to read but also reinforces the brand
                              feeling.</p>
                    </div> <!-- cd-timeline-content -->
               </div>
               <div class="cd-timeline-block counter-1">
                    <div class="cd-timeline-img cd-picture">
                         <img class="circle-no-line"
                              src="<?php echo get_template_directory_uri(); ?>/images/circle-no-line.svg" alt="Circle">
                         <img src="<?php echo get_template_directory_uri(); ?>/images/circle-left-line.svg"
                              alt="Circle">
                    </div> <!-- cd-timeline-img -->
                    <div class="cd-timeline-content">
                         <img src="<?php echo get_template_directory_uri(); ?>/images/graphic-design-icon.svg"
                              alt="Image">
                         <h2>Graphic Design</h2>
                         <p>Illustrations and other graphic elements are designed and added into the layout. Graphics
                              are especially powerful for visual communication.</p>
                    </div> <!-- cd-timeline-content -->
               </div>
          </div>
     </section>

     <div id="timeline_bottom" class="container container_timeline_bottom">
          <div class="timeline_bottom">
               <div class="timeline_bottom_title">
                    <h3>Still finding this a bit confusing? Don’t worry, we’re here to help.</h3>
                    <img src="<?php echo get_template_directory_uri(); ?>/images/timeline-bottom-line.svg" alt="Image">
               </div>

               <ul class="list-unstyled timeline_bottom_repeater">
                    <li>
                         <div>
                              <h4>
                                   UX (low-res)
                              </h4>
                              <p>Take one of our lovely case studies, to design this page we start with the UX: the
                                   information architecture and wireframe, sometimes referred to the bones or skeleton
                                   of a webpage.</p>
                         </div>
                         <img src="<?php echo get_template_directory_uri(); ?>/images/UX-image-updated.png"
                              alt="UX (low-res)">
                    </li>
                    <li>
                         <div>
                              <h4>
                                   UI (high-res)
                              </h4>
                              <p>Once the wireframe has been approved, we can start implementing the UI, such as
                                   typography, colour palette and imagery.</p>
                         </div>
                         <img src="<?php echo get_template_directory_uri(); ?>/images/UI-image-updated.png"
                              alt="UI (high-res)">
                    </li>
               </ul>
          </div>
     </div>

     <div class="wrapper wrapper-single-resource">
          <?php
          if ( have_posts() ) {while ( have_posts() ) {the_post();
                    the_content();
          }}?>
     </div>
</main>


<?php get_footer(); ?>