<?php

//Allowing featured image in posts
add_theme_support('post-thumbnails');

//Add page title
add_theme_support('title-tag');

//Bring back menus
add_theme_support('menus');

//Remove commenting section
add_action('admin_init', function () {
	// Redirect any user trying to access comments page
	global $pagenow;

	if ($pagenow === 'edit-comments.php') {
		wp_redirect(admin_url());
		exit;
	}

	// Remove comments metabox from dashboard
	remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');

	// Disable support for comments and trackbacks in post types
	foreach (get_post_types() as $post_type) {
		if (post_type_supports($post_type, 'comments')) {
			remove_post_type_support($post_type, 'comments');
			remove_post_type_support($post_type, 'trackbacks');
		}
	}
});

// Close comments on the front-end
add_filter('comments_open', '__return_false', 20, 2);
add_filter('pings_open', '__return_false', 20, 2);

// Hide existing comments
add_filter('comments_array', '__return_empty_array', 10, 2);

// Remove comments page in menu
add_action('admin_menu', function () {
	remove_menu_page('edit-comments.php');
});

// Remove comments links from admin bar
add_action('init', function () {
	if (is_admin_bar_showing()) {
		remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
	}
});
//END remove comment section

/* Register widget areas. */
function widget_registration()
{

	// Arguments used in all register_sidebar() calls.
	$shared_args = array(
		/*'before_title'  => '<h2 class="widget-title subheading heading-size-3">',
		'after_title'   => '</h2>',*/
		'before_widget' => '',
		'after_widget'  => '',
	);
	// Footer #1.
	register_sidebar(
		array_merge(
			$shared_args,
			array(
				'name'        => __('Footer Left'),
				'id'          => 'sidebar-1',
				'description' => __('Widgets in this area will be displayed in the left column in the footer.'),
			)
		)
	);
	// Footer #2.
	register_sidebar(
		array_merge(
			$shared_args,
			array(
				'name'        => __('Footer Right'),
				'id'          => 'sidebar-2',
				'description' => __('Widgets in this area will be displayed in the second column in the footer.'),
			)
		)
	);
}

add_action('widgets_init', 'widget_registration');

/* Menu locations */
function lc_menus()
{
	$locations = array(
		'primary'  => __('Main Menu'),
		'footer'  => __('Footer Menu'),
	);
	register_nav_menus($locations);
}
add_action('init', 'lc_menus');


//Page Slug Body Class
function add_slug_body_class($classes)
{
	global $post;
	if (isset($post)) {
		$classes[] = $post->post_type . '-' . $post->post_name;
	}
	return $classes;
}
add_filter('body_class', 'add_slug_body_class');

// Enable SVG
// Make sure that each SVG file starts with: <?xml version="1.0"...... etc
function cc_mime_types($mimes)
{
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

// Make SVG visible in media upload
function custom_admin_head()
{
	$css = '';
	$css = 'td.media-icon img[src$=".svg"] { width: 100% !important; height: auto !important; }';
	echo '<style type="text/css">' . $css . '</style>';
}
add_action('admin_head', 'custom_admin_head');

//Our Work Custom Post Type
function our_work_custom_post_type()
{
	// Set UI labels for Custom Post Type
	$labs_labels = array(
		'name'                => _x('Our Work', 'Post Type General Name'),
		'singular_name'       => _x('Our Work', 'Post Type Singular Name'),
		'menu_name'           => __('Our Work'),
		'parent_item_colon'   => __('Parent Our Work'),
		'all_items'           => __('All Our Work'),
		'view_item'           => __('View Our Work'),
		'add_new_item'        => __('Add New Our Work'),
		'add_new'             => __('Add New'),
		'edit_item'           => __('Edit Our Work'),
		'update_item'         => __('Update Our Work'),
		'search_items'        => __('Search Our Work'),
		'not_found'           => __('Not Found'),
		'not_found_in_trash'  => __('Not found in Trash'),
	);

	// Set other options for Custom Post Type
	$labs_args = array(
		'label'               => __('our-work'),
		'description'         => __('Our Work'),
		'labels'              => $labs_labels,
		'supports'            => array('title', 'editor', 'thumbnail', 'revisions', 'excerpt'),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'rewrite' => array('slug' => 'our-work', 'with_front' => false),
		'menu_icon'           => 'dashicons-portfolio',
		'menu_position'       => 10,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'show_in_rest'        => true,
	);

	// Registering your Custom Post Type
	register_post_type('our-work', $labs_args);
}
add_action('init', 'our_work_custom_post_type');

add_action('init', 'our_work_taxonomy', 0);
function our_work_taxonomy()
{
	$labels = array(
		'name' => _x('Categories Our Work', 'taxonomy general name'),
		'singular_name' => _x('Category Our Work', 'taxonomy singular name'),
		'search_items' =>  __('Search Categories Our Work'),
		'all_items' => __('All Categories Our Work'),
		'parent_item' => __('Parent Category Our Work'),
		'parent_item_colon' => __('Parent Category Our Work:'),
		'edit_item' => __('Edit Category Our Work'),
		'update_item' => __('Update Category Our Work'),
		'add_new_item' => __('Add New Category Our Work'),
		'new_item_name' => __('New Category Our Work Name'),
		'menu_name' => __('Categories Our Work'),
	);

	register_taxonomy('our-work-category', array('our-work'), array(
		'hierarchical' => true,
		'labels' => $labels,
		'show_ui' => true,
		'show_admin_column' => true,
		'show_in_rest' => true,
		'query_var' => true,
		'rewrite' => array('slug' => 'our-work-category'),
	));
}

// Register Jobs Post Type
function jobs_custom_post_type()
{

	$labels = array(
		'name'                  => 'Jobs',
		'singular_name'         => 'Job',
		'menu_name'             => 'Jobs',
		'name_admin_bar'        => 'Jobs',
		'archives'              => 'Job Archives',
		'attributes'            => 'Item Attributes',
		'parent_item_colon'     => 'Parent Item:',
		'all_items'             => 'All Jobs',
		'add_new_item'          => 'Add New Job',
		'add_new'               => 'Add Job',
		'new_item'              => 'New Job',
		'edit_item'             => 'Edit Job',
		'update_item'           => 'Update Job',
		'view_item'             => 'View Job',
		'view_items'            => 'View Jobs',
		'search_items'          => 'Search Jobs',
		'not_found'             => 'Not found',
		'not_found_in_trash'    => 'Not found in Trash',
		'featured_image'        => 'Featured Image',
		'set_featured_image'    => 'Set featured image',
		'remove_featured_image' => 'Remove featured image',
		'use_featured_image'    => 'Use as featured image',
		'insert_into_item'      => 'Insert into item',
		'uploaded_to_this_item' => 'Uploaded to this item',
		'items_list'            => 'Items list',
		'items_list_navigation' => 'Items list navigation',
		'filter_items_list'     => 'Filter items list',
	);
	$args = array(
		'label'                 => 'Job',
		'description'           => 'Current jobs we are recruiting for',
		'labels'                => $labels,
		'supports'              => array('title', 'editor', 'revisions', 'thumbnail'),
		'taxonomies'            => array('category', 'post_tag'),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'show_in_rest'			=> true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type('jobs', $args);
}
add_action('init', 'jobs_custom_post_type', 0);

// Register Podcasts Post Type
function podcast_custom_post_type()
{

	$labels = array(
		'name'                  => 'Podcasts',
		'singular_name'         => 'Podcast',
		'menu_name'             => 'Podcasts',
		'name_admin_bar'        => 'Podcasts',
		'archives'              => 'Podcast Archives',
		'attributes'            => 'Item Attributes',
		'parent_item_colon'     => 'Parent Item:',
		'all_items'             => 'All Podcasts',
		'add_new_item'          => 'Add New Podcast',
		'add_new'               => 'Add Podcast',
		'new_item'              => 'New Podcast',
		'edit_item'             => 'Edit Podcast',
		'update_item'           => 'Update Podcast',
		'view_item'             => 'View Podcast',
		'view_items'            => 'View Podcasts',
		'search_items'          => 'Search Podcasts',
		'not_found'             => 'Not found',
		'not_found_in_trash'    => 'Not found in Trash',
		'featured_image'        => 'Featured Image',
		'set_featured_image'    => 'Set featured image',
		'remove_featured_image' => 'Remove featured image',
		'use_featured_image'    => 'Use as featured image',

	);
	$args = array(
		'label'                 => 'Podcast',
		'description'           => 'All podcasts',
		'labels'                => $labels,
		'show_in_rest' => true,
		'supports'              => array('title', 'editor', 'revisions', 'thumbnail', 'excerpt',),
		'taxonomies'            => array('category', 'post_tag'),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type('podcasts', $args);
}
add_action('init', 'podcast_custom_post_type', 0);


// Register Resources Post Type
function resource_custom_post_type()
{

	$labels = array(
		'name'                  => 'Resources',
		'singular_name'         => 'Resource',
		'menu_name'             => 'Resources',
		'name_admin_bar'        => 'Resources',
		'archives'              => 'Resource Archives',
		'attributes'            => 'Item Attributes',
		'parent_item_colon'     => 'Parent Item:',
		'all_items'             => 'All Resources',
		'add_new_item'          => 'Add New Resource',
		'add_new'               => 'Add Resource',
		'new_item'              => 'New Resource',
		'edit_item'             => 'Edit Resource',
		'update_item'           => 'Update Resource',
		'view_item'             => 'View Resource',
		'view_items'            => 'View Resources',
		'search_items'          => 'Search Resources',
		'not_found'             => 'Not found',
		'not_found_in_trash'    => 'Not found in Trash',
		'featured_image'        => 'Featured Image',
		'set_featured_image'    => 'Set featured image',
		'remove_featured_image' => 'Remove featured image',
		'use_featured_image'    => 'Use as featured image',

	);
	$args = array(
		'label'                 => 'Resource',
		'description'           => 'All Resources',
		'labels'                => $labels,
		'show_in_rest' => true,
		'supports'              => array('title', 'editor', 'revisions', 'thumbnail', 'excerpt',),
		'taxonomies'            => array('category', 'post_tag'),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type('resources', $args);
}
add_action('init', 'resource_custom_post_type', 0);
// Removing the default excerpt styling
function custom_excerpt_more($excerpt)
{
	return ' ...';
}
add_filter('excerpt_more', 'custom_excerpt_more');




/* ACF BLOCKS */

//Added a custom block category named "Drew & Rose" which contains all own gutenberg blocks
function custom_block_drew_category($categories)
{
	$custom_block = array(
		'slug'  => 'drew-rose',
		'title' => __('Drew & Rose Blocks', 'drew-rose')
	);
	$categories_sorted = array();
	$categories_sorted[0] = $custom_block;
	foreach ($categories as $category) {
		$categories_sorted[] = $category;
	}
	return $categories_sorted;
}
add_filter('block_categories', 'custom_block_drew_category');

// Blocks
add_action('acf/init', 'my_acf_init_block_types');
function my_acf_init_block_types()
{
	if (function_exists('acf_register_block_type')) {
		acf_register_block_type(array(
			'name'              => 'sub-menu',
			'title'             => __('Sub Menu block'),
			'description'       => __('A custom Sub Menu block'),
			'render_template'   => 'blocks/sub-menu-block.php',
			'category'          => 'drew-rose',
			'keywords'          => array('sub-menu'),
			'example'  => array(
				'attributes' => array(
					'mode' => 'preview',
					'data' => array(
						'is_preview'    => true
					)
				)
			)
		));
	}
}
//ACTIVATED OPTIONS ACF PAGE
if (function_exists('acf_add_options_page')) {
	acf_add_options_page();
}
