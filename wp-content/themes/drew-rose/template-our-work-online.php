<?php
/*
     Template Name: Our Work - Online
*/

get_header();
?>

<main id="site-content" role="main">

     <!-- HERO -->
     <div class="wp-block-group hero desktop">
          <div class="wp-block-group__inner-container">
               <div class="wp-block-columns">
                    <div class="wp-block-column">
                         <h1 class="page-title">Our work</h1>
                         <div class="our-work-categories">
                              <!-- <?php if( $groups = get_terms( array( 'taxonomy' => 'our-work-category', 'hide_empty' => false ) ) ) : ?>
                                   <ul class="groups-list list-unstyled">
                                        <?php foreach( $groups as $group ) : ?>
                                        <li><?php echo $group->name ?></li>
                                        <?php endforeach; ?>
                                   </ul>
                              <?php endif; ?> -->
                              <ul class="groups-list list-unstyled">
                                   <li>
                                        <a href="<?php echo get_site_url(); ?>/our-work-offline" title="Offline">
                                             <span class="sr-only">Offline - Brand | Website</span>
                                             <img src="<?php echo get_template_directory_uri(); ?>/images/our-work/offline-category.svg"
                                                  alt="Offline">
                                        </a>
                                   </li>
                                   <li class="active">
                                        <a href="<?php echo get_site_url(); ?>/our-work-online" title="Online">
                                             <span class="sr-only">Digital Marketing Journeys | Conversion
                                                  Funnels</span>
                                             <img src="<?php echo get_template_directory_uri(); ?>/images/our-work/online-category.svg"
                                                  alt="Online">
                                        </a>
                                   </li>
                                   <li>
                                        <a href="<?php echo get_site_url(); ?>/our-work-in-real-life"
                                             title="In real life">
                                             <span class="sr-only">In Real Life - Events | Campaigns | Video</span>
                                             <img src="<?php echo get_template_directory_uri(); ?>/images/our-work/in-real-life-category.svg"
                                                  alt="In real life">
                                        </a>
                                   </li>
                              </ul>
                         </div>
                    </div>
               </div>



          </div>
     </div>


     <div class="wp-block-group case-studies">
          <div class="wp-block-group__inner-container">
               <img src="<?php echo get_template_directory_uri(); ?>/images/ow-online.svg" class="mobile intro-title">
               <?php
               $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                    $query = new WP_Query( array(
                    'post_type' => 'our-work',
                    'tax_query' => array(
                         array(
                         'taxonomy' => 'our-work-category',
                         'field'    => 'slug',
                         'terms'    => 'online',
                         ),
                    ),
                    'post_status'  => 'publish',
                    'orderby' =>  'date',
                    'order' =>  'DESC',
                    'posts_per_page' => -1
                    ) );
               ?>

               <?php if ( $query->have_posts() ) : $count = 0; ?>

               <?php while ( $query->have_posts() ) : $query->the_post();
                    $count ++;
                    $even_odd_class = ( ($count % 2) == 0 ) ? "column-lines-container-2 even" : "column-lines-container-1 odd";
                    $image_border_class = ( ($count % 2) == 0 ) ? "curved-img-border-left" : "curved-img-border-right";
               ?>
               <div class="wp-block-group <?php echo $even_odd_class; ?> desktop">
                    <div class="wp-block-group__inner-container">
                         <div class="wp-block-columns">
                              <div class="wp-block-column">
                                   <div class="wp-block-group  <?php echo $image_border_class; ?>">
                                        <div class="wp-block-group__inner-container">
                                             <?php if( has_post_thumbnail() ) { ?>
                                             <figure class="wp-block-image size-full">
                                                  <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                                       <?php the_post_thumbnail('large'); ?>
                                                  </a>
                                             </figure>
                                             <?php } ?>
                                        </div>
                                   </div>
                              </div>
                              <div class="wp-block-column copy">
                                   <div class="wrapper-copy">
                                        <h2><a href="<?php the_permalink(); ?>"
                                                  title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
                                        <p> <?php
                                                  if( has_excerpt() ) {
                                                       echo wp_trim_words(get_the_excerpt(), 14, '..' );
                                                  }else echo wp_trim_words( get_the_content(), 14, '..' );
                                             ?>

                                        </p>
                                        <div class="wp-block-buttons">
                                             <div class="wp-block-button">
                                                  <a href="<?php the_permalink(); ?>" class="wp-block-button__link">View
                                                       Case
                                                       Study</a>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
               <div class="wp-block-columns mobile">
                    <div class="wp-block-column">
                         <div class="wp-block-group  <?php echo $image_border_class; ?>">
                              <div class="wp-block-group__inner-container">
                                   <figure class="wp-block-image size-full">
                                        <a href="<?php the_permalink(); ?>">
                                             <?php
                                             if( has_post_thumbnail() ) {
                                                  the_post_thumbnail('large');
                                             }
                                             ?>
                                        </a>
                                   </figure>
                              </div>
                         </div>
                    </div>
                    <div class="wp-block-column copy">
                         <div class="wrapper-copy">
                              <h2><a href="<?php the_permalink(); ?>"
                                        title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
                         </div>
                    </div>
               </div>

               <?php endwhile; ?>

               <?php wp_reset_postdata(); ?>

               <?php else : ?>
               <div class="alert alert-warning">
                    <?php _e( 'Sorry, no posts matched your criteria.' ); ?>
               </div>
               <?php endif; ?>

          </div>
     </div>


     <?php
	if ( have_posts() ) {while ( have_posts() ) {
               the_post();
			the_content();
	}}?>
</main>


<?php get_footer(); ?>