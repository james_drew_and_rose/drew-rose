				<a href="mailto:enquires@drewandrose.com" class="fixme"><img src="<?php echo get_template_directory_uri(); ?>/images/click-me.svg" alt="email"></a>
	<footer>

			<div class="footer-socials">
				
				<div class="footer-menu">
				<?php
					wp_nav_menu(
					array(
					'items_wrap' => '%3$s',
					'theme_location' => 'footer',
					)
					);
						?>
				</div>
				
				<div class="contact-us-footer">
					<h2>Contact us</h2>
					<a class="footer-phone-call" href="tel:02030115950" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/phone-footer-icon.svg" alt="Phone icon">020 3011 5950</a>
					<a class="footer-email" href="mailto:enquires@drewandrose.com" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/footer-email.svg" alt="Email icon">enquires@drewandrose.com</a>
					<a class="footer-submit-a-brief" href="https://drewandrose.typeform.com/to/J6jLD3Vl" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/footer-submit-brief.svg" alt="submit a brief">Tell us what you need</a>
				</div>
				
				<div class="sign-up">
					<h2>Connect with us</h2>
					<div class="img-cont">
						<a href="https://www.instagram.com/drewandroseagency/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/insta.svg" alt="instagram"></a>
						<a href="https://twitter.com/Drew_and_Rose" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/twitter.svg" alt="twitter"></a>
						<a href="https://www.facebook.com/drewrandroseagency/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/facebook.svg" alt="facebook"></a>
						<a href="https://www.linkedin.com/company/drewroseagency" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/linkedin.svg" alt="linkedin"></a>
					</div>
				</div>

			</div>
		
			<div class="footer-links">
				<a href="<?php echo site_url(); ?>/privacy-policy/" class="item1">Privacy Policy</a>
				<a href="<?php echo site_url(); ?>/terms-conditions/" class="item2">Terms + Conditions</a>
				<a href="" class="item3">©<?php echo date("Y"); ?> Copyright Drew + Rose</a>

			</div>

			<!--Mobile links-->
			<div class="img-cont-mob">
				<a href="https://www.instagram.com/drewandroseagency/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/insta.svg" alt="instagram"></a>
				<a href="https://twitter.com/Drew_and_Rose" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/twitter.svg" alt="twitter"></a>
				<a href="https://www.facebook.com/drewrandroseagency/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/facebook.svg" alt="facebook"></a>
				<a href="https://www.linkedin.com/company/drewroseagency" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/linkedin.svg" alt="linkedin"></a>
			</div>

	</footer>
	<?php wp_footer(); ?>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/scripty.js" type="text/css" /></script>
	</body>
</html>