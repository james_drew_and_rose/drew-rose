<?php

/*
    Template Name: Standard empty page
*/

get_header();
?>
<main id="site-content" role="main">
    <style>
        main{background-color:white;padding-top: 100px;}
        .white-logo{display: none;}
        .colour-logo{display: block;}
    </style>
    <div class="wrapper">
    	<?php
    	if ( have_posts() ) {while ( have_posts() ) {the_post();

                the_title( '<h1 class="entry-title">', '</h1>' );
                echo( '<div class="content-section">' );
    			the_content();
                echo( '</div>' );
                
    	}}?>
    </div>
</main>


<?php get_footer(); ?>