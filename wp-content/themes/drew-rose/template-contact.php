<?php
/*
	Template Name: Contact Us
*/

get_header();
?>

<main id="site-content" role="main">



	<!-- HERO -->
	<div class="wp-block-group contact-us-heading">
		<div class="wp-block-group__inner-container">
		<img src="<?php echo get_template_directory_uri(); ?>/images/stars-2.svg" class="stars-img">
			<h1 class="page-title">Talk to us</h1>
			<p class="subheading">Got a Marcoms challenge that’s keeping you up at night?<br class='desktop'> Talk to Elle and Charlotte over a cuppa or a cocktail.<br><br>Get in touch and we’ll do the rest.</p>
			<div class="main-img-cont">
			<img src="<?php echo get_template_directory_uri(); ?>/images/char-elle.png" class="char-elle-img">
			</div>
			<div class="wp-block-columns main-info">
				
				<div class="wp-block-column email-column">
					<h2>Email us</h2>
					<a class="email-link" href="mailto:enquires@drewandrose.com">enquires@drewandrose.com</a>
					<a class="email-icon" target="_blank" href="mailto:enquires@drewandrose.com">
					  <div class="icon"></div>
						</a>
				</div>
				
				<div class="wp-block-column">
					<h2>Submit a brief</h2>
					<p>Let's talk this week?</p>
					<a class="brief-icon icon" target="_blank" href="https://drewandrose.typeform.com/to/J6jLD3Vl">
					   <div class="icon"></div>
					</a>
				</div>
				<div class="wp-block-column meeting-column">
					<h2>Set up a meeting</h2>
					<p>Looking to change agency and want to set up a meeting/call?</p>
					<a class="meeting-icon icon" target="_blank" href="https://meetings.hubspot.com/elle-moss">
					   <div class="icon"></div>
					</a>
				</div>
			</div>
		</div>
	</div>

	 <!-- HERO -->
	<div class="wp-block-group page-contact-us-footer">
		<div class="wp-block-group__inner-container">
			<div class="wp-block-columns ">
				<div class="wp-block-column">
					<div class="contact-details">
						<a href="tel:+44 (0) 20 3011 5950">+44 (0) 20 3011 5950</a>
						
						<a href="mailto:enquires@drewandrose.com">enquires@drewandrose.com</a>
						<br>
					</div>
					<div class="office-adress">
						<div class="office-address-flexbox">
							<div class="column-office-1">
								<h4>Head Office</h4>
							<p>Office 2.07<br>179 Great Portland Street,<br>London, W1W 5PL</p>  
							</div>
							<div class="column-office-2">
									<h4>Brixton Office</h4>
								<p>3Space International House,<br>6 Canterbury Crescent,<br>Brixton, SW9 7QD</p>
							</div>
						</div>
					</div>
				    <div class="contact-us-social">
					 <a href="https://www.instagram.com/drewandroseagency/" target="_blank"><div class="icon instagram"></div></a>
					 <a href="https://www.facebook.com/drewrandroseagency/" target="_blank"><div class="icon facebook"></div></a>
					 <a href="https://twitter.com/Drew_and_Rose" target="_blank"><div class="icon twitter"></div></a>
					 <a href="https://www.linkedin.com/company/drewroseagency" target="_blank"><div class="icon linkedin"></div></a>
				    </div>
				</div>
				
			</div>
		</div>
	</div>

</main>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/scripty.js" type="text/css" /></script>
