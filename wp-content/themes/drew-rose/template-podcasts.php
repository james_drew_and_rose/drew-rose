<?php
/*
     Template Name: Podcasts
*/

get_header();
?>

<section class="blog-resouces-podcasts-ctn resources-main">

     <!-- HERO -->
     <div class="wp-block-group hero">
          <div class="wp-block-group__inner-container">
               <div class="wp-block-columns">
                    <div class="wp-block-column">
                         <h1 class="hidden-page-title">Podcasts</h1>
                         <div class="insights-hero">
                              <?php echo file_get_contents( get_template_directory_uri() . '/images/insights-podcasts.svg' ); ?>
                         </div>
                    </div>
               </div>



          </div>
     </div>


     <div class="wp-block-group blog-ctn podcast-ctn">
          <?php if ( have_posts() ) {while ( have_posts() ) { the_post(); ?>

          <?php the_content(); ?>

          <?php }}?>

          



          <!-- Commenting custom post type code
          <div class="wp-block-group__inner-container">
               <?php $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                    $query = new WP_Query( array(
                    'post_type' => 'podcasts',
                    'post_status'  => 'publish',
                    'orderby' =>  'title',
                    'order' =>  'ASC',
                    'posts_per_page' => -1
                    ) );
               ?>

               <?php if ( $query->have_posts() ) : $count = 0; ?>

               <?php while ( $query->have_posts() ) : $query->the_post();
                    $count ++;
                    $even_odd_class = ( ($count % 2) == 0 ) ? "column-lines-container-2 even" : "column-lines-container-1 odd";
                    $image_border_class = ( ($count % 2) == 0 ) ? "curved-img-border-left" : "curved-img-border-right";
               ?>

               <div class="single-blog-item">
                    <?php if ( has_post_thumbnail() ) : ?>
                    <div class="card-img-top blog-img-col"
                         style="background-image:url(<?php the_post_thumbnail_url(); ?>);">
                    </div>
                    <?php endif; ?>
                    <div class="content-col">
                         <p class="blog-date"><?php echo get_the_date(); ?></p>
                         <h2><?php the_title(); ?></h2>
                         <?php the_excerpt(); ?>
                    </div>
               </div>

               <?php endwhile; ?>

               <?php wp_reset_postdata(); ?>

               <?php else : ?>
               <div class="alert alert-warning">
                    <?php _e( 'Sorry, no posts matched your criteria.' ); ?>
               </div>
               <?php endif; ?> -->
     </div>
     </div>


</section>

<?php get_footer(); ?>