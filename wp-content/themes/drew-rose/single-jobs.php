<?php
/*
Template Name: Single Post
*/

global $wp;
$current_url = home_url(add_query_arg(array($_GET), $wp->request));

get_header();
?>



<div class="jobs-content wp-block-group">

     <?php while (have_posts()) : the_post(); ?>
     <?php the_content(); ?>
     <?php endwhile; ?>

</div>
 



<?php get_footer(); ?>