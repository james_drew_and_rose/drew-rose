<?php
/*
     Template Name: Our Team
*/

get_header();
?>
<main id="site-content" role="main">
    <div class="wrapper">
    	<?php
    	if ( have_posts() ) {while ( have_posts() ) {the_post();

    			/*the_title( '<h1 class="entry-title">', '</h1>' );*/
    			the_content();
    			the_post_thumbnail( 'full' );

    	}}?>
    </div>
</main>


<?php get_footer(); ?>